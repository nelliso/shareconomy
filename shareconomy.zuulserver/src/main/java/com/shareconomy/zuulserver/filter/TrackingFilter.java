package com.shareconomy.zuulserver.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

@Component
public class TrackingFilter extends ZuulFilter {

	private static final int FILTER_ORDER = 1;
	private static final boolean MUST_BE_FILTERED = true;
	private static final Logger LOGGER = LoggerFactory.getLogger(TrackingFilter.class);

	@Autowired
	private FilterUtils filterUtils;

	@Override
	public Object run() {
		if (isCorrelationIdPresent()) {
			LOGGER.debug("correlation-id found in tracking filter: {}", filterUtils.getCorrelationId());
		} else {
			filterUtils.setCorrelationId(generateCorrelationId());
			LOGGER.debug("correlation-id generated in tracking filter: {}", filterUtils.getCorrelationId());
		}
		
		RequestContext context = RequestContext.getCurrentContext();
		LOGGER.debug("processing incoming request for {}", context.getRequest().getRequestURI()); 
		
		return null;
	}

	@Override
	public boolean shouldFilter() {
		return MUST_BE_FILTERED;
	}

	@Override
	public int filterOrder() {
		return FILTER_ORDER;
	}

	@Override
	public String filterType() {
		return FilterUtils.PRE_FILTER_TYPE;
	}

	private boolean isCorrelationIdPresent() {
		if (filterUtils.getCorrelationId() != null) {
			return true;
		}

		return false;
	}

	public String generateCorrelationId() {
		return java.util.UUID.randomUUID().toString();
	}

}
