package com.shareconomy.zuulserver.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

@Component
public class ResponseFilter extends ZuulFilter{

	private static final int FILTER_ORDER = 1;
	private static final boolean MUST_BE_FILTERED = true;
	private static final Logger LOGGER = LoggerFactory.getLogger(ResponseFilter.class);

	@Autowired
	private FilterUtils filterUtils;

	@Override
	public Object run() {
		RequestContext context = RequestContext.getCurrentContext();
		
		LOGGER.debug("Add the correlation id to the outbound headers. {}", filterUtils.getCorrelationId());
		context.getResponse().addHeader(FilterUtils.CORRELATION_ID, filterUtils.getCorrelationId());
		
		LOGGER.debug("Completing outgoing request for {}", context.getRequest().getRequestURI());
		
		return null;
	}

	@Override
	public boolean shouldFilter() {
		return MUST_BE_FILTERED;
	}

	@Override
	public int filterOrder() {
		return FILTER_ORDER;
	}

	@Override
	public String filterType() {
		return FilterUtils.POST_FILTER_TPYE;
	}

}
