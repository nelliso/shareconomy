package com.shareconomy.productservice.controller;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.shareconomy.productservice.model.Product;
import com.shareconomy.productservice.model.User;
import com.shareconomy.productservice.service.AccountService;
import com.shareconomy.productservice.service.ProductService;
import com.shareconomy.productservice.util.UserContextHolder;

@RestController
@RequestMapping("/se/product")
public class ProductServiceController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceController.class);

	@Autowired
	private ProductService productService;
	
	@Autowired
	private AccountService accountService;
	
	@RequestMapping(method = RequestMethod.POST)
	public Product addProduct(@RequestBody Product prod){
		String username = "ntege";
		
		return productService.addProductByUser(prod, username);
	}
	
	@RequestMapping("/user/products")
	public List<Product> getAllProductsByUser(){
		String username = "ntege";
		
		return productService.getProductsByUsername(username);
	}
	
	@RequestMapping("/{id}")
	public Product getProductById(@PathVariable Long id){
		return productService.getProductById(id);
	}
	
	@RequestMapping("/all")
	public List<Product> getAllProducts(){
		return productService.getAllProducts();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Product updateProduct(@PathVariable Long id, @RequestBody Product product) throws IOException{
		product.setId(id);
		return productService.updateProduct(product);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteByProductId(@PathVariable Long id) throws IOException{
		productService.deleteProductById(id);
	}
	
	@RequestMapping("/account/{username}")
	public User getUserByUsername(@PathVariable String username){
		LOGGER.debug("--- ProductServiceController Correlation id: {}", 
				UserContextHolder.getContext().getCorrelationId());
		
		return productService.getUserByUsername(username);
	}
	
}
