package com.shareconomy.productservice.hystrix;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.netflix.hystrix.strategy.HystrixPlugins;
import com.netflix.hystrix.strategy.concurrency.HystrixConcurrencyStrategy;
import com.netflix.hystrix.strategy.eventnotifier.HystrixEventNotifier;
import com.netflix.hystrix.strategy.executionhook.HystrixCommandExecutionHook;
import com.netflix.hystrix.strategy.metrics.HystrixMetricsPublisher;
import com.netflix.hystrix.strategy.properties.HystrixPropertiesStrategy;

@Configuration
public class ThreadLocalConfiguration {

	@Autowired(required = false)
	private HystrixConcurrencyStrategy hystrixConcurrencyStrategy;
	
	@PostConstruct
	public void init(){
		HystrixEventNotifier notifier = HystrixPlugins.getInstance().getEventNotifier();
		HystrixMetricsPublisher publisher = HystrixPlugins.getInstance().getMetricsPublisher();
		HystrixPropertiesStrategy strategy = HystrixPlugins.getInstance().getPropertiesStrategy();
		HystrixCommandExecutionHook executionHook = HystrixPlugins.getInstance().getCommandExecutionHook();
		
		HystrixPlugins.reset();
		
		HystrixPlugins.getInstance().registerConcurrencyStrategy(new ThreadLocalAwareStrategy(hystrixConcurrencyStrategy));
		HystrixPlugins.getInstance().registerEventNotifier(notifier);
		HystrixPlugins.getInstance().registerMetricsPublisher(publisher);
		HystrixPlugins.getInstance().registerPropertiesStrategy(strategy);
		HystrixPlugins.getInstance().registerCommandExecutionHook(executionHook);
		
	}
}
