package com.shareconomy.productservice.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.shareconomy.productservice.model.Product;
import com.shareconomy.productservice.model.User;

@Repository
@Transactional
public interface ProductRepository extends CrudRepository<Product, Long>{
	
	List<Product> findByUser(User user);
	Product findByName(String name);
	
}
