package com.shareconomy.productservice.dao;

import org.springframework.data.repository.CrudRepository;

import com.shareconomy.productservice.model.User;

public interface AccountRepository extends CrudRepository<User, Long>{
	User findByUsername(String username);
}
