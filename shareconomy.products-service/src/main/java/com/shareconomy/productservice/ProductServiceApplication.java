package com.shareconomy.productservice;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.sleuth.Sampler;
import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import com.shareconomy.productservice.model.Product;
import com.shareconomy.productservice.model.User;
import com.shareconomy.productservice.service.AccountService;
import com.shareconomy.productservice.service.ProductService;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EnableResourceServer
@EnableCircuitBreaker
public class ProductServiceApplication implements CommandLineRunner{
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private AccountService accountService;
	
	@Bean
	public Sampler defaultSampler(){
		return new AlwaysSampler();
	}

	@Bean
	public OAuth2RestTemplate oauth2RestTemplate(OAuth2ClientContext context, 
			OAuth2ProtectedResourceDetails details) {
		return new OAuth2RestTemplate(details, context);
	}
	
	public static void main(String[] args){
		SpringApplication.run(ProductServiceApplication.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		User user = accountService.getUserByUsername("henry");
		
		Product product1 = new Product();
		product1.setName("Item1");
		product1.setProductCondition("New");
		product1.setStatus("Active");
		product1.setAddDate(new Date());
		product1.setDescription("item description.");
		product1.setUser(user);
		
		productService.addProductByUser(product1, user.getUsername());
		
		Product product2 = new Product();
		product2.setName("product2");
		product2.setProductCondition("Used");
		product2.setStatus("Inactive");
		product2.setAddDate(new Date());
		product2.setDescription("This is an item description for product2.");
		product2.setUser(user);
		
		productService.addProductByUser(product2, user.getUsername());
		
	}
}
