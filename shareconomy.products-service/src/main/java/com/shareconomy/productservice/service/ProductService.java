package com.shareconomy.productservice.service;

import java.io.IOException;
import java.util.List;

import com.shareconomy.productservice.model.Product;
import com.shareconomy.productservice.model.User;

public interface ProductService {

	Product addProductByUser(Product product, String username);
	List<Product> getAllProducts();
	List<Product> getProductsByUsername(String username);
	Product getProductById(Long id);
	Product updateProduct(Product product) throws IOException;
	void deleteProductById(Long id);
	
	User getUserByUsername(String username);
}
