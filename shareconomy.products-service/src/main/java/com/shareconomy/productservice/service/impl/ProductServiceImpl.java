package com.shareconomy.productservice.service.impl;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shareconomy.productservice.client.AccountRestTemplateClient;
import com.shareconomy.productservice.dao.ProductRepository;
import com.shareconomy.productservice.model.Product;
import com.shareconomy.productservice.model.User;
import com.shareconomy.productservice.service.AccountService;
import com.shareconomy.productservice.service.ProductService;
import com.shareconomy.productservice.util.UserContextHolder;

@Service
public class ProductServiceImpl implements ProductService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceImpl.class);
	
	@Autowired
	private ProductRepository productRepo;
	
	@Autowired
	private AccountService accountService;
	
//	@Autowired
//	private AccountFeignClient accountFeignClient;
	
	@Autowired
	private AccountRestTemplateClient accountRestTemplateClient;
	
	
	@Override
	public Product addProductByUser(Product product, String username) {
		Product productExists = productRepo.findByName(product.getName());
		
		if(productExists != null){
			LOGGER.info("product with name {} already exists. ", productExists.getName());
			
			return null;
		}else{
			Date now = new Date();
			product.setAddDate(now);
			
			User user = accountService.getUserByUsername(username);
			product.setUser(user);
			Product newProduct = productRepo.save(product);
			
			return newProduct;
		}
		
	}

	@Override
	public List<Product> getAllProducts() {
		return (List<Product>) productRepo.findAll();
	}

	@Override
	public List<Product> getProductsByUsername(String username) {
		User user = accountService.getUserByUsername(username);
		
		return productRepo.findByUser(user);
	}

	@Override
	public Product getProductById(Long id) {
		return productRepo.findOne(id);
	}

	@Override
	public Product updateProduct(Product product) throws IOException {
		Product productExists = getProductById(product.getId());
		
		if(productExists == null){
			throw new IOException("Product was found.");
		}else{
			productExists.setName(product.getName());
			productExists.setProductCondition(product.getProductCondition());
			productExists.setStatus(product.getStatus());
			productExists.setDescription(product.getDescription());
			
			return productRepo.save(productExists);
		}
		
	}

	@Override
	public void deleteProductById(Long id) {
		productRepo.delete(id);
	}

	@Override
//	@HystrixCommand(
//			fallbackMethod = "fallBackAccount", 
//			threadPoolKey = "itemByUserThreaPool",
//			threadPoolProperties = {
//					@HystrixProperty(name="coreSize", value="30"),
//					// (requests per second at peak when the service is healthy * 99th percentile latency in seconds) +
//					// small amount of extra threads for overhead
//					@HystrixProperty(name="maxQueueSize", value="10")
//			}
//	)
	public User getUserByUsername(String username) {
//		randomlyRunLong();
		
		LOGGER.debug("--- ProductService.getUserByUsername Correlation id: {}", UserContextHolder.getContext().getCorrelationId());
		
//		return accountFeignClient.getUserByUsername(username);
		return accountRestTemplateClient.getUser(username);
	}
	
	// invoking circuit breaker to sleep
	private void randomlyRunLong(){
		Random rand = new Random();
		int randomNum = rand.nextInt((3-1)+1)+1;
		
		if(randomNum == 3 ) sleep();
	}
	
	private void sleep() {
		try{
			Thread.sleep(11000);
		}catch(InterruptedException e){
			e.printStackTrace();
		}
	}
	
	private User fallBackAccount(String username){
		User user = new User();
		user.setId(765L);
		user.setUsername("backup name");
		
		return user;
	}

}
