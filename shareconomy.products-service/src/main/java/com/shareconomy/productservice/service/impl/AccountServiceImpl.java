package com.shareconomy.productservice.service.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shareconomy.productservice.dao.AccountRepository;
import com.shareconomy.productservice.model.Role;
import com.shareconomy.productservice.model.User;
import com.shareconomy.productservice.model.UserRole;
import com.shareconomy.productservice.service.AccountService;
import com.shareconomy.productservice.util.SecurityUtil;

@Service
public class AccountServiceImpl implements AccountService{
	
	private final static Logger LOGGER = LoggerFactory.getLogger(AccountServiceImpl.class);
	
	@Autowired
	private AccountRepository userRepository;

	@Override
	public User getUserByUsername(String username) {
		return userRepository.findByUsername(username);
	}
}
