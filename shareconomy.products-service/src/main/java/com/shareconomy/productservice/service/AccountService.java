package com.shareconomy.productservice.service;

import com.shareconomy.productservice.model.User;

public interface AccountService {
	
	User getUserByUsername(String username);
}
