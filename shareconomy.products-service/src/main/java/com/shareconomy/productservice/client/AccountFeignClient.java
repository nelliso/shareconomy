/*
package com.shareconomy.productservice.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.shareconomy.productservice.model.User;

@FeignClient("accountservice") // 'accountservice' is the service app name in AccountService
public interface AccountFeignClient {

	@RequestMapping(value = "/se/account/{username}", 
			consumes = "application/json" ) // MediaType.APPLICATION_JSON_VALUE 
	User getUserByUsername(@PathVariable("username") String username);
}
*/
