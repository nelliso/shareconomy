package com.shareconomy.productservice.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Component;

import com.shareconomy.productservice.model.User;
import com.shareconomy.productservice.util.UserContextHolder;

@Component
public class AccountRestTemplateClient {
	
	@Autowired
	private OAuth2RestTemplate oAuth2RestTemplate;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AccountRestTemplateClient.class);
	
	public User getUser(String username){
		LOGGER.debug("in product service.getUser: {}", UserContextHolder.getContext().getCorrelationId());
		
		ResponseEntity<User> entity = oAuth2RestTemplate.exchange("http://localhost:7777/api/account/se/account/{username}", 
				HttpMethod.GET, null, User.class, username);
		
		return entity.getBody();
	}
	
}
