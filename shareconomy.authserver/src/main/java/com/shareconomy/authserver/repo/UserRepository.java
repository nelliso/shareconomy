package com.shareconomy.authserver.repo;

import org.springframework.data.repository.CrudRepository;

import com.shareconomy.authserver.entities.User;

public interface UserRepository extends CrudRepository<User, Long> {

	User findByUsername(String username);
}
