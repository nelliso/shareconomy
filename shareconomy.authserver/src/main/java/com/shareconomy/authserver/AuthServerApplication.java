package com.shareconomy.authserver;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableAuthorizationServer
@EnableEurekaClient
@EnableResourceServer
public class AuthServerApplication {
        
        private static final Logger LOGGER = LoggerFactory.getLogger(AuthServerApplication.class);
        
	@RequestMapping(value = "/user", /*consumes = MediaType.APPLICATION_JSON_VALUE*/ produces = "application/json")
	public Map<String, Object> user(OAuth2Authentication authentication){
                
		LOGGER.debug("--- Entering the AUTH SERVER ------ ");
	
            
    
		Map<String, Object> userInfo = new HashMap<>();
		userInfo.put("user", authentication.getUserAuthentication().getPrincipal());
		userInfo.put("authorities", AuthorityUtils.authorityListToSet(authentication.getUserAuthentication().getAuthorities()));
		
		return userInfo;
	}
	
	public static void main(String[] args){
		SpringApplication.run(AuthServerApplication.class, args);
	}
}
