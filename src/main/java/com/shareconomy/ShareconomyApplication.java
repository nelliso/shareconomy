package com.shareconomy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShareconomyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShareconomyApplication.class, args);
	}
}
