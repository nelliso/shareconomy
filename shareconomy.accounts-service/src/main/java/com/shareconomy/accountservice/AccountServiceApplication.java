package com.shareconomy.accountservice;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.sleuth.Sampler;
import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import com.shareconomy.accountservice.model.Role;
import com.shareconomy.accountservice.model.User;
import com.shareconomy.accountservice.model.UserRole;
import com.shareconomy.accountservice.service.AccountService;

@SpringBootApplication
@EnableEurekaClient
// @EnableFeignClients
// @EnableCircuitBreaker
@EnableResourceServer
public class AccountServiceApplication implements CommandLineRunner{
	
	@Autowired
	private AccountService accountService; 
	
	@Bean
	public Sampler defaultSampler(){
		return new AlwaysSampler();
	}
	
	public static void main(String[] args){
		SpringApplication.run(AccountServiceApplication.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		User user = new User();
		user.setFirstName("Henry");
		user.setLastName("Ezra");
		user.setUsername("henry");
		user.setPassword("tester12A##");
		user.setEmail("henry@gmail.com");
		
		Set<UserRole> userRoles = new HashSet<>();
		Role role1 = new Role();
		role1.setRoleId(1);
		role1.setName("ROLE_USER");
		userRoles.add(new UserRole(user, role1));
		
		accountService.createUser(user);
		
	}
}
