package com.shareconomy.accountservice.util;

import java.security.SecureRandom;
import java.util.Random;

import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class SecurityUtil {
	
	private static final String SALT = "kamuli";
	
	@Bean
	public static BCryptPasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder(15, new SecureRandom(SALT.getBytes()));
	}
	
	@Bean
	public static String randomizePassword(){
		String spiceSalt = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder builder = new StringBuilder();
		Random random = new Random();
		
		while(builder.length() < 18){
			int x = (int)(random.nextFloat() * spiceSalt.length());
			builder.append(spiceSalt.charAt(x));
		}
		
		String strg = builder.toString();
		
		return strg;
	}
	
}
