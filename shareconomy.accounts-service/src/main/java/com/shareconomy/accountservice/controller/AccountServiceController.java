package com.shareconomy.accountservice.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.shareconomy.accountservice.model.User;
import com.shareconomy.accountservice.service.AccountService;

@RestController
@RequestMapping("/se/account")
public class AccountServiceController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AccountServiceController.class);
	@Autowired
	private AccountService userService;
	
	@RequestMapping("/{username}")
	public User getUserByUsername(@PathVariable("username") String username){
		LOGGER.debug("Entering the account-service-controller");
		
		return userService.getUserByUsername(username);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public User createUser(@RequestBody User user){
		return userService.createUser(user);
	}
}
