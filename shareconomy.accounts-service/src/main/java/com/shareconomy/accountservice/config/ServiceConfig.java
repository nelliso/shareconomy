package com.shareconomy.accountservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfig {

	@Value("${locking.key}")
	private String jwtLockingKey = "";

	public String getJwtLockingKey() {
		return jwtLockingKey;
	}
	
}
