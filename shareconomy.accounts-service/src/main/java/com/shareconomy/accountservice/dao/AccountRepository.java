package com.shareconomy.accountservice.dao;

import org.springframework.data.repository.CrudRepository;

import com.shareconomy.accountservice.model.User;

public interface AccountRepository extends CrudRepository<User, Long>{
	User findByUsername(String username);
}
