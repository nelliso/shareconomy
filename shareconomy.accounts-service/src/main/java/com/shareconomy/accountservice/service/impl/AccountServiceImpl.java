package com.shareconomy.accountservice.service.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shareconomy.accountservice.dao.AccountRepository;
import com.shareconomy.accountservice.model.Role;
import com.shareconomy.accountservice.model.User;
import com.shareconomy.accountservice.model.UserRole;
import com.shareconomy.accountservice.service.AccountService;
import com.shareconomy.accountservice.util.SecurityUtil;

@Service
public class AccountServiceImpl implements AccountService{
	
	private final static Logger LOGGER = LoggerFactory.getLogger(AccountServiceImpl.class);
	
	@Autowired
	private AccountRepository userRepository;

	@Override
	public User createUser(User user) {
		User existingUser = userRepository.findByUsername(user.getUsername());
		
		if(existingUser != null){
			LOGGER.info("User with username {} already exists. Nothing will be done.", user.getUsername());
		}else{
			Set<UserRole> userRole = new HashSet<>();
			Role role = new Role();
			role.setRoleId(1);
			userRole.add(new UserRole(user, role));
			user.getUserRoles().addAll(userRole);
			
			Date now = new Date();
			user.setJoinDate(now);
			
			String ecryptedPWD = SecurityUtil.passwordEncoder().encode(user.getPassword());
			user.setPassword(ecryptedPWD);
			existingUser = userRepository.save(user);
		}
		
		return existingUser;
	}

	@Override
	public User getUserByUsername(String username) {
		return userRepository.findByUsername(username);
	}
}
