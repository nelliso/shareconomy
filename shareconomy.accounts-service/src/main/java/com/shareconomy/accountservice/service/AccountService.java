package com.shareconomy.accountservice.service;

import com.shareconomy.accountservice.model.User;

public interface AccountService {
	
	User createUser(User user);
	
	User getUserByUsername(String username);
}
